import seaborn as sns
import matplotlib.pyplot as plt
plt.style.use('matplotlib_style.txt')
import pandas as pd
from matplotlib.ticker import MaxNLocator, MultipleLocator
import networkx as nx


def plot_case_and_background_GDVs(GDVs_case, GDVs_background):
    """ 
    """

    assert GDVs_case.shape[1] == 15, "The number of orbits in the case GDVs should be 15."
    assert GDVs_background.shape[1] == 15, "The number of orbits in the background GDVs should be 15."

    orbits = ['o0', 'o1', 'o2', 'o3', 'o4', 'o5', 'o6', 'o7', 'o8', 'o9', 'o10', 'o11', 'o12', 'o13', 'o14']
    fig, ax = plt.subplots(figsize=(5, 3))

    df_case = pd.DataFrame(GDVs_case, columns=orbits)
    df_case = pd.melt(df_case, var_name='orbit', value_name='count')
    df_case['status'] = 'case'

    df_background = pd.DataFrame(GDVs_background, columns=orbits)
    df_background = pd.melt(df_background, var_name='orbit', value_name='count')
    df_background['status'] = 'background'

    df = pd.concat([df_case, df_background], axis=0)

    sns.lineplot(data=df, x='orbit', y='count', hue='status', dashes=False,
                 ax=ax, errorbar=("pi", 50))
    
    # remove minor x ticks
    ax.xaxis.set_minor_locator(plt.NullLocator())  

    ax.grid(axis='y')
    plt.subplots_adjust(bottom=0.20)
    ax.legend(loc='lower center', bbox_to_anchor=(0.5, -.4), ncol=2, title=None)
    return ax

def plot_embedding(embedding, driver_status):
    """ Plot the embeddings """

    embedding = embedding[:, :2]

    labels = ['driver' if status == 1 else 'background' for status in driver_status]
    
    plt.figure(figsize=(5, 3))
    df = pd.DataFrame(embedding, columns=['x', 'y'])
    sns.scatterplot(data=df, x='x', y='y', hue=labels, palette=['orange', 'blue'])

    return plt.gca()


def plot_two_distributions(dist1, dist2):
    """ Plot two distributions on the same graph. """

    fig, ax = plt.subplots(figsize=(5, 3))

    df_case = pd.DataFrame(dist1, columns=['distance'])
    df_case['status'] = 'driver2driver'
    df_background = pd.DataFrame(dist2, columns=['distance'])
    df_background['status'] = 'driver2background'
    df = pd.concat([df_case, df_background], axis=0)
    ax=sns.histplot(df,x='distance', hue='status', ax=ax)
    ax.grid(axis='y')
    plt.subplots_adjust(bottom=0.20)
    sns.move_legend(ax, "lower center", bbox_to_anchor=(0.5, -.4), ncol=2, title=None)
    # plt.legend(loc='upper right')
    # ax.legend(loc='lower center', bbox_to_anchor=(0.5, -.4), ncol=2, title=None)

    return ax

def spring_embedding(G):

    nx.draw_spring(G, font_weight='bold')
