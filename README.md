# Tutorial graphlet-based topological network analysis

## Installation 

We will create a virtual environment with all the necessary packages installed. 
There are two ways to do this: using either `conda` or `pip`.


### Using `conda`

To create an environment with all the necessary packages installed, clone this
repositry and from the project root directory run:

```
conda create -f env.yml
```

To activate the environment run:

```
conda activate belbi_tutorial
```

### Using `pip`

Using this approach requires that you have python 3.10 or higher installed on your system.

To create a virtual environment with all the necessary packages installed, run the following commands:

```
python3.10 -m venv belbi_tutorial
source belbi_tutorial/bin/activate
pip install -r requirements.txt
```

## Running the notebook

To run the notebook, run the following command:

```
jupyter lab
```

