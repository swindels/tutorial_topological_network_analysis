import argparse
import numpy as np
from utils import map_symbol_to_entrez
import networkx as nx
import pandas as pd


def main():
    parser = argparse.ArgumentParser(description="Intogen")
    parser.add_argument("net_file", help="Input file")
    parser.add_argument("intogen_file", help="Input file")
    parser.add_argument("out_file", help="Output file")

    args = parser.parse_args()

    G = nx.read_edgelist(args.net_file, nodetype=str)

    df = pd.read_csv(args.intogen_file, sep="\t")
    symbols = df["SYMBOL"].values
    entrez = set(map_symbol_to_entrez(symbols))

    labels = np.zeros(G.number_of_nodes(), dtype=bool)
    for i, node in enumerate(G.nodes()):
        if node in entrez:
            labels[i] = 1

    np.savetxt(args.out_file, labels, fmt="%d")


if __name__ == "__main__":
    main()
