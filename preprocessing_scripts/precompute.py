import argparse
import gradco
import networkx as nx
from scipy import sparse
import numpy as np

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("net_file", type=str)
    args = parser.parse_args()

    G = nx.read_edgelist(args.net_file, nodetype=str)
    A = nx.to_scipy_sparse_array(G)
    counter = gradco.Counter(A)
    counter.count()

    GDVs = counter.get_GDVs()

    net_name = args.net_file.split("/")[-1].split(".")[0]
    GDV_file = f"precomputed_features/GDVs/GDVs_{net_name}.txt"
    np.savetxt(GDV_file, GDVs)

   
    # store the graphlet adjacencies
    for graphlet, A in enumerate(counter.generate_graphlet_adjacencies()):
        print("GA:", graphlet)
        GA_file = f"precomputed_features/GAs/GA_{net_name}_{graphlet}.npz"
        sparse.save_npz(GA_file, A)
        if graphlet == 2:
            break

    # store the orbit adjacencies
    for hop, o1, o2, A in counter.generate_orbit_adjacencies():
        if o1 <3 and o2 <3:
            print("O:", hop, o1, o2)
            O_file = f"precomputed_features/OAs/OA_{net_name}_{hop}_{o1}_{o2}.npz"
            sparse.save_npz(O_file, A)


if __name__ == "__main__":
    main()
