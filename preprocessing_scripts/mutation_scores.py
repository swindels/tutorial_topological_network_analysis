import argparse
import numpy as np
from utils import map_symbol_to_entrez
import networkx as nx
import pandas as pd


def main():
    parser = argparse.ArgumentParser(description="Intogen")
    parser.add_argument("net_file", help="Input file")
    parser.add_argument("mutationscores_file", help="Input file")
    parser.add_argument("out_file", help="Output file")

    args = parser.parse_args()

    G = nx.read_edgelist(args.net_file, nodetype=str)

    df = pd.read_csv(args.mutationscores_file, sep=" ",
                     header=None, names=["SYMBOL", "SCORE"])
    symbols = df["SYMBOL"].values
    entrez = map_symbol_to_entrez(symbols)

    entrez2score = dict(zip(entrez, df["SCORE"].values))

    labels = np.zeros(G.number_of_nodes(), dtype=float)

    for i, node in enumerate(G.nodes()):
        if node in entrez2score:
            labels[i] = entrez2score[node]

    np.savetxt(args.out_file, labels)


if __name__ == "__main__":
    main()
