import pandas as pd
import numpy as np


def map_entrez_to_symbol(entrez_ids):
    """
    Map entrez gene IDs to gene symbols.
    Entrez IDs can be for human or yeast genes.
    """
    df_human = pd.read_csv(
        'data/gene_id_maps/biomart_human_gene_id_map.txt', dtype={'entrez': str})
    df_yeast = pd.read_csv(
        'data/gene_id_maps/biomart_yeast_gene_id_map.txt', dtype={'entrez': str})

    df = pd.concat([df_human, df_yeast])
    df = df.drop_duplicates()
    # drop rows with missing values
    df = df.dropna()

    id_map = dict(zip(df.entrez, df.symbol))

    # map entrez IDs to gene symbols
    gene_symbols = []
    for gene_id in entrez_ids:
        if gene_id in id_map:
            gene_symbols.append(id_map[gene_id])
        else:
            gene_symbols.append("NA")
    return gene_symbols


def map_symbol_to_entrez(symbols):
    """
    Map gene symbols to entrez gene IDs
    """
    df_human = pd.read_csv(
        'data/gene_id_maps/biomart_human_gene_id_map.txt', dtype={'entrez': str})
    df_yeast = pd.read_csv(
        'data/gene_id_maps/biomart_yeast_gene_id_map.txt', dtype={'entrez': str})
    df = pd.concat([df_human, df_yeast])
    df = df.drop_duplicates()
    # drop rows with missing values
    df = df.dropna()

    id_map = dict(zip(df.symbol, df.entrez))

    # map gene symbols to entrez IDs
    entrez_ids = []
    for symbol in symbols:
        if symbol in id_map:
            entrez_ids.append(id_map[symbol])
        else:
            entrez_ids.append("NA")
    return entrez_ids

def compute_GDV_similarity_matrix(GDVs):
    """ Compute the similarity matrix of an array of graphlet degree vectors (GDVs).

    parameters:
    -----------
    GDVs: np.ndarray (n x 14)
        Array of graphlet degree vectors (GDVs) for n nodes and 14 orbits.

    returns:
    --------
    A: np.ndarray (n x n)
        Similarity matrix of the GDVs.

    """

    n = GDVs.shape[0]
    A = np.ones((n, n))
    # TODO 1: compute the similarity matrix of the GDVs.
    # For efficiency, note that GDV_sim(gdv1, gdv2) == GDV_sim(gdv2, gdv1)
    for i in range(n):
        for j in range(i+1, n):
            A[i, j] = compute_GDV_similarity(GDVs[i,:], GDVs[j,:])
            A[j, i] = A[i, j]

    return A
